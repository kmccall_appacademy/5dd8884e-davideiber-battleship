class BattleshipGame
  attr_reader :board, :player

  def initialize(player=Player.new, board=Board.new)
    @board = board
    @player = player
  end

  def attack(pos)
    if @board.grid[pos[0]][pos[1]] == :s
      @board.grid[pos[0]][pos[1]] = :x
    elsif @board.grid[pos[0]][pos[1]].nil?
      @board.grid[pos[0]][pos[1]] = :-
    end
  end

  def count
    @board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    location = player.get_play
    attack(location)
  end

  def endgame_message
    puts "All the enemy ships have been destroyed."
  end

  def play
    puts "Press enter."
    gets
    while game_over? == false
      puts "There are #{board.count} ship tiles remaining."
      fired_board = board.grid
      player.display(fired_board)
      player.prompt
      play_turn
    end

    if game_over?
      player.display(board.grid)
      endgame_message
    end
  end
end
